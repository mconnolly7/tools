
% This is merged version of TDT and Neurorighter loading code
% Usage: data = Load_LFP(filename) or data = Load_LFP()

function out = Load_LFP(filename)

if ~exist('filename')
    [f, p] = uigetfile('*.*', 'MultiSelect', 'on');
    if iscell(f) == 0
        f = {f};
        p = {p};
    end
    for i=1:1:length(f)
        [f1{i}, f2{i}, ext{i}] = fileparts(f{i});
        filename{i} = strcat(p,f{i});
    end
else
%     filename = {filename};
    for i=1:1:length(filename)
        [f1{i}, f2{i}, ext{i}] = fileparts(filename{i});
    end
end

for i = 1:1:length(f1)
    switch ext{i}
        case '.raw'
            [out{i}.data, out{i}.time] = MasterLoadraw(char(filename{i}));
        case '.lfp'
            [out{i}.data, out{i}.time] = MasterLoadraw(char(filename{i}));
        case '.olaux'
            [out{i}.time, out{i}.channel, out{i}.voltage]=readolauxfile(char(filename{i}));
        case '.aux'
            [out{i}.data, out{i}.time] = MasterLoadraw(char(filename{i}));
        case '.spk'
            [out{i}.spike] = MasterLoadSpike(char(filename{i}));
        case '.eeg'
            [out{i}.data, out{i}.time] = MasterLoadraw(char(filename{i}));
        case '.mat'
            out{i} = load(char(filename{i}));
    end
    out{i}.filename = char(filename{i});
    fprintf('%d / %d file is loading\n',i,length(filename));
end