function [y]=MasterLoadDig(filename)
% This is a master loaddig function that will run through the current and
% previous versions of the loaddig script. It will output a correct version
% of the data, if one can be found.  
% 
%   MASTERLOADDIG reads *.dig files created by the NeuroRighter software.
%      y = LOADDIG(filename) takes as input the filename of a NeuroRighter
%      DIG file and returns a struct containing:
%           time          [1 N] vector of times of a digital event
%                               (in seconds)
%           state         [1 N] vector of 32-bit integers representing the 
%                               port state at the corresponding time
%   
% 
%   Created by: Nealen Laxpati (nlaxpat@emory.edu)
%   Created on: May 6, 2013
%   Last modified: May 6, 2013
%   Note that this function uses loaddig functions originally created by
%   Jon Newman and found in the NeuroRighter code repository.
%  
%   Licensed under the GPL: http://www.gnu.org/licenses/gpl.txt
%  
%

% Potential additional inputs:
% 1) Manually select the version you want to use



%% Errors and Number of Inputs

load=1; %Start the loading process. This will serve as a break point so that I can skip the other loading processes if it works correctly

%% First use the latest version of loaddig (loaddig_v110542)
while load==1;
    %Trying to load the file
    try
        
        y=loaddig_v110542(filename);
             
        
        test=sum(isnan(y.time))+sum(isinf(y.time));
        
        if test==0;
            load=0;
            break;
        else
            fprintf('\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\nLoaddig_v110542 failed, trying deprecated versions\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n');
            clear y test
        end
        
    catch
        fprintf('\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\nLoaddig_v110542 failed, trying deprecated versions\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n');
    end
    

    
    %% Now I'm testing the previous versions of loadstim, which is loadstim_v3
    try
        
        y=loaddig_v3(filename);
        
        
        
        test=sum(isnan(y.time))+sum(isinf(y.time));
        
        if test==0;
            load=0;
            break;
        else
            fprintf('\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\nLoaddig_v3 failed, there are no more versions to try\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n');
            clear y test
        end
        
    catch
        fprintf('\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\nLoaddig_v3 failed, there are no more versions to try\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n');
    end


    load=0;
end

end