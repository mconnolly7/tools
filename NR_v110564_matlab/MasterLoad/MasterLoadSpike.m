function [SPK]=MasterLoadSpike(filename,varargin)
% This is a master loadspike function that will run through the current and
% previous versions of the loadspike script (loadspike and loadspk). It will output a correct version
% of the data, if one can be found.  
% 
% For files from NeuroRighter version 0.7.0.0 and up (loadspk):
%     MASTERLOADSPIKE Load NeuroRighter .spk,.rawspk, and .salpaspk files for NR
%       v0.7.0.0 and up
% 
%       SPK = MASTERLOADSPIKE(FID) fid is the fully qualified path to a NeuroRighter .spk
%       file. The structure that is returned contains metadata about the spike
%       data and the spike data itself.
% 
%       SPK = MASTERLOADSPIKE(FID,TIME,WAVE) loads the .spk file for spikes that occured
%       between time = [t0 t1) on the channels specified in chan. Wave is a
%       boolean that specifies whether waveforms should be returned. Use empty
%       brackets, [], to specify defaults.
% 
%       T = MASTERLOADSPIKE(FID,'last') returns the final spike time in the file. This is
%       useful setting up a loop that parses the file into time chunks.
% 
%       T = MASTERLOADSPIKE(FID,'head') prints the file header to the console and
%       returns file meta data only.
% 
% For files from NeuroRighter versions below 0.7.0.0 (loadspike):
%     MASTERLOADSPIKE *.spk files created by the NeuroRighter software.
%       y = MASTERLOADSPIKE(filename) takes as input the filename of a NeuroRighter
%       spike file and returns a struct containing:
%            channel     [1 N] vector of channels on which a spike occured
%            time        [1 N] vector of spike times (in seconds)
%            threshold   [1 N] vector of thresholds used to detect each spike
%            waveform    [M N] matrix of clipped spike waveforms, scaled to Volts
%  
%       y = MASTERLOADSPIKE(filename, channel) returns data only from the specified
%       channel
%  
%       y = MASTERLOADSPIKE(filename, channel, [t0 t1]) returns data from the 
%       specified channel within the specified time range (>= t0 and <= t1).
%   
%       y = MASTERLOADSPIKE(filename, [t0 t1]) returns data within the specified 
%       time range (as above).
% 
%   
%   Created by: Nealen Laxpati (nlaxpat@emory.edu)
%   Created on: May 5, 2013
%   Last modified: May 5, 2013
%   Note that this function uses loadspike functions created by John
%   Rolston (rolston2@gmail.com) and loadspk functions created by Jon
%   Newman, and which can be found in the Neurorighter code repository.
%  
%   Licensed under the GPL: http://www.gnu.org/licenses/gpl.txt
%  
%

% Potential additional inputs:
% 1) Manually select the version you want to use



%% Errors and Number of Inputs

error(nargchk(0,3,nargin))

if nargin>1
    FLAG1=varargin{1};
    if nargin>2
        FLAG2=varargin{2};
    end
end

load=1; %Start the loading process. This will serve as a break point so that I can skip the other loading processes if it works correctly

%% First use the latest version of loadspk (loadspk_v1100)
while load==1;
    %Trying to load the file
    try
        if nargin==3;
            SPK=loadspk_v1100(filename,FLAG1,FLAG2);
        else if nargin==2;
                SPK=loadspk_v1100(filename,FLAG1);
            else if nargin==1;
                    SPK=loadspk_v1100(filename);
                end
            end
        end
        
        %From what I can tell, there will be errors if the wrong type of
        %file is attempted. So there shouldn't be a need to test it as I
        %did in MasterLoadraw
        disp([filename ' was successfully loaded with loadspk_v1100.']);
        break; %If it is successful, then it will stop the loop
        
    catch
        fprintf('\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\nloadspk_v1100 failed, trying deprecated versions\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n');
    end
    

    
    %% Now I'm testing the previous versions of loadspk, which is loadspk_v0700
    try
        if nargin==3;
            SPK=loadspk_v0700(filename,FLAG1,FLAG2);
        else if nargin==2;
                SPK=loadspk_v0700(filename,FLAG1);
            else if nargin==1;
                    SPK=loadspk_v0700(filename);
                end
            end
        end
        
        disp([filename ' was successfully loaded with loadspk_v0700.']);
        break; %If it is successful, then it will stop the loop
        
        
    catch
        fprintf('\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\nloadspk_v0700 failed, trying another deprecated version\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n');
    end

%% Now I'm testing the earliest versions of loadspk, which is loadspike_v0
    try
        if nargin==3;
            SPK=loadspike_v0(filename,FLAG1,FLAG2);
        else if nargin==2;
                SPK=loadspike_v0(filename,FLAG1);
            else if nargin==1;
                    SPK=loadspike_v0(filename);
                end
            end
        end
        
        disp([filename ' was successfully loaded with loadspike_v0.']);
        break;
        
    catch
        fprintf('\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\nloadspike_v0 failed, there are no more versions to try\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n');
    end    
    
    load=0;
end

end