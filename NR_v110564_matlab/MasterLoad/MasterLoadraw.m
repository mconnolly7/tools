function [y,t]=MasterLoadRaw(filename,varargin)
% This is a master loadraw function that will run through the current and
% previous versions of the loadraw script. It will output a correct version
% of the data, if one can be found.  
% 
% MASTERLOADRAW Reads *.raw or *.lfp files created by the NeuroRighter software.
%  
%   y = MASTERLOADRAW(filename) takes as input the filename of a NeuroRighter
%       raw or LFP file and returns traces in an MxN matrix, where M  
%       is the number of channels and N is the number of data points.
%  
%   y = MASTERLOADRAW(filename, ch) extracts only the specified channel (1-based)
%  
%   y = MASTERLOADRAW(filename, timespan) extracts all channel data
%       for the specific time range.  'timespan' is a 1x2 vector, [t0 t1].
%       Samples in the range t0 <= t < t1 are extracted.
%  
%   y = MASTERLOADRAW(filename, ch, timespan) extracts data from the specified
%       channel for the specified time range.  Setting ch = -1 extracts
%       all channels.
%  
%   y = MASTERLOADRAW(filename, ch, timespan, FLAG1, FLAG2, ...) uses FLAGs to
%       define additional options.  Available flags are:
%           1) SuppressText
%  
%   [y, t] = MASTERLOADRAW(filename, ...) additionally returns the time stamps of
%       the acquired samples in a 1xN vector (in seconds).
%   
%   Created by: Nealen Laxpati (nlaxpat@emory.edu)
%   Created on: April 30, 2013
%   Last modified: May 6, 2013
%   Note that this function uses the loadraw functions created by John
%   Rolston (rolston2@gmail.com) and modofied by Jon Newman, and found in
%   the Neurorighter code repository.
%  
%   Licensed under the GPL: http://www.gnu.org/licenses/gpl.txt
%  
%

% Potential additional inputs:
% 1) Manually select the version you want to use



%% Errors and Number of Inputs

error(nargchk(0,3,nargin));

if nargin>1
    ch=varargin{1};
    if nargin>2
        timespan=varargin{2};
        if nargin>3
            FLAG1=varargin{3};
        end
    end
end

load=1; %Start the loading process. This will serve as a break point so that I can skip the other loading processes if it works correctly

%% First use the latest version of loadraw
while load==1;
    %Trying to load the file
    try
        if nargin==3;
            [y,t]=loadraw_v110542(filename,ch,timespan);
        else if nargin==2;
                [y,t]=loadraw_v110542(filename,ch);
            else if nargin==1;
                    [y,t]=loadraw_v110542(filename);
                end
            end
        end
        
        %Testing the file for it's authenticity
        test=sum(isnan(t))+sum(isinf(t));
        
        if test==0;
            load=0;
            break;
        else
            fprintf('\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\nLoadraw_v110542 failed, trying deprecated versions\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n');
            clear y t test
        end
        
    catch
        fprintf('\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\nLoadraw_v110542 failed, trying deprecated versions\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n');
    end
    

    
%% Now I'm testing the previous versions of loadraw, which is loadraw_v3
    try
        if nargin==3;
            [y,t]=loadraw_v3(filename,ch,timespan);
        else if nargin==2;
                [y,t]=loadraw_v3(filename,ch);
            else if nargin==1;
                    [y,t]=loadraw_v3(filename);
                end
            end
        end
  
        
        %Testing the file for it's authenticity
        test=sum(isnan(t))+sum(isinf(t));
        
        if test==0;
            load=0;
            break;
        else
             fprintf('\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\nLoadraw_v3 failed, o longer any versions to try\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n');
        end
    catch
        disp('Loadraw_v3 failed, no longer any versions to try');
    end

load=0;    
end

end