NeuroRighter Matlab Files (Master Version 1. For NeuroRighter v1.1.0.564 and up)

There are a number of different versions of the Matlab loading files for NeuroRighter, and each is dependent upon the version of NeuroRighter you are using. In order to streamline the loading process, there are four "Master" matlab functions that can load NeuroRighter's binary data files into matlab structs from any previous version of NeuroRighter:
1) MasterLoadRaw*: loads raw or LFP files( *.raw, *.salpa, *.spkflt, or *.aux)
2) MasterLoadSpike: loads spk waveform data( *.spk, *.rawspk, *.salpaspk)
3) MasterLoadStim: loads stimulation data (*.stim)
4) MasterLoadDig: loads digital recordings (*.dig)

For usage of each function, type �help [function name]� at the Matlab command prompt.  For example, �help masterloadraw�.  

The current (v1.1.0.564) and deprecated versions of Neurorighter have to be renamed properly to work with these scripts. Where possible, I have renamed the versions according to the revision in which they were changed. Here are the changes that need to be made:
	1) Loadraw
		Top level rennamed to loadraw_v110542.m*
		Deprecated Version 3 rennamed to loadraw_v3.m*
	2) LoadSpike
		Top Level rennamed to loadspk_v1100.m
		Deprecated Version 3 rennamed to loadspk_v0700.m
		Deprecated Version 2 rennamed to loadspike_v0.m
	3) LoadStim
		Top Level rennamed to loadstim_v110542.m
		Deprecated Version 3 rennamed to loadstim_v3.m

	4) LoadDig
		Top Level rennamed to loaddig_v110542.m
		Deprecated Version 3 rennamed to loaddig_v3.m


*To use MasterLoadRaw.m:loadraw is a MEX function, written in C, and compiled to work from within Matlab.  If it does not not work when initially installing NeuroRighter, type �mex [function file name]� at the  Matlab command prompt, where [function file name] is the *.c file for the desired function.  For  example, �mex loadraw_v110542.c�.  You will need to do this for each version of loadraw. This should produce a working function. If you are having issues with this process, please see this this webpage http://www.mathworks.com/help/matlab/matlab_external/building-mex-files.html and then email the user's list if you still cannot figure it out.



>> 2012.10.31: notes on version 4

- The big change in this version of the matlab loading files is that an error concerning the sampling rate has been fixed. Before this version of the Matlab Functions, all sampling rates in NeuroRighter were being rounded to a natural number. This could cause issues for very long recordings because the true sampling rate is some multiple of NI card's master clock, which could result in a fracion. Therefore, sampling rates are now doubles. However, this means that the new versions of the scripts are incompatible with old data containing the integer sampling rate. Deprecated scripts (versions 2 and 3) are included in the 'Deprecated' folder and can be used to look at older data.


This document was created by John Rolston (rolston2@gmail.com) on Jan. 29, 2009.  It was last  modified on May 6, 2013 by Nealen Laxpati (nlaxpat <at> emory <dot> edu).
