function [y]=MasterLoadStim(filename)
% This is a master loadstim function that will run through the current and
% previous versions of the loadstim script. It will output a correct version
% of the data, if one can be found.  
% 
%   MASTERLOADSTIM reads *.stim files created by the NeuroRighter software.
%      y = LOADSTIM(filename) takes as input the filename of a NeuroRighter
%      stim file and returns a struct containing:
%           channel     [1 N] vector of channels on which a stim pulse occured
%           time        [1 N] vector of stim times (in seconds)
%           voltage     [1 N] vector of max. voltage of deliver stim. waveforms, scaled to Volts
%           pulse width [1 N] vector of pulse widths (in microseconds)
%   
% 
%   Created by: Nealen Laxpati (nlaxpat@emory.edu)
%   Created on: May 6, 2013
%   Last modified: May 6, 2013
%   Note that this function uses loadstim functions originally created by
%   John Rolston (rolston2@gmail.com) and Jon Newman, and which can be
%   found in the Neurorighter Code Repository.
%  
%   Licensed under the GPL: http://www.gnu.org/licenses/gpl.txt
%  
%

% Potential additional inputs:
% 1) Manually select the version you want to use



%% Errors and Number of Inputs

load=1; %Start the loading process. This will serve as a break point so that I can skip the other loading processes if it works correctly

%% First use the latest version of loadstim (loadstim_v110542)
while load==1;
    %Trying to load the file
    try
        
        y=loadstim_v110542(filename);
        
        
        
        %From what I can tell, there will be errors if the wrong type of
        %file is attempted. So there shouldn't be a need to test it as I
        %did in MasterLoadraw
        test=sum(isnan(y.time))+sum(isinf(y.time));
        
        if test==0;
            load=0;
            break;
        else
            fprintf('\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\nLoadstim_v110542 failed, trying deprecated versions\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n');
            clear y test
        end
        
    catch
        fprintf('\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\nLoadstim_v110542 failed, trying deprecated versions\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n');
    end
    

    
    %% Now I'm testing the previous versions of loadstim, which is loadstim_v3
    try
        
        y=loadstim_v3(filename);
        
        
        
        test=sum(isnan(y.time))+sum(isinf(y.time));
        
        if test==0;
            load=0;
            break;
        else
            fprintf('\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\nLoadstim_v3 failed, there are no more versions to try\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n');
            clear y test
        end
        
    catch
        fprintf('\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\nLoadstim_v3 failed, there are no more versions to try\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n');
    end


    load=0;
end

end