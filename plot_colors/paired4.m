function output_color = paired4(color_idx)

c= [166,206,227
178,223,138
31,120,180
51,160,44
]/255;

% c= [215,25,28
% 253,174,97
% 171,217,233
% 44,123,182]/255;
if nargin == 0
    output_color = c;
else
    output_color = c(color_idx,:);
end
end

