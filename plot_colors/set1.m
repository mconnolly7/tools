function output_color = set1(color_idx)


c = [228,26,28
55,126,184
77,175,74
152,78,163
255,127,0
255,255,51
166,86,40
247,129,191
153,153,153]/255;

if nargin == 0
    output_color = c;
else
    output_color = c(color_idx,:);
end

end

