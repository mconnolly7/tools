function output_color = dark2(color_idx)

c = [27,158,119
217,95,2
117,112,179
231,41,138
102,166,30
230,171,2
166,118,29
102,102,102 ]/255;

if nargin == 0
    output_color = c;
else
    output_color = c(color_idx,:);
end
end

